﻿using Library.Repository.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Library.Core.Trade
{
    public class PilingBaseProcessor
    {
        #region protected area
        protected DateTime _initialSasDateTime = DateTime.Parse("1960-1-1");
        protected bool _sqlDebug = true;
        protected string _lastSqlDebugString = string.Empty;
        protected int GetDaysForSAS(DateTime? dt)
        {
            var days = 0;
            if (dt.HasValue)
                days = (dt.Value.Date - _initialSasDateTime).Days;
            return days;
        }
        protected int GetSecsForSAS(DateTime? dt)
        {
            var secs = 0;
            if (dt.HasValue)
                secs = dt.Value.Hour * 3600 + dt.Value.Minute * 60 + dt.Value.Second;
            return secs;
        }

        protected void SetLastSqlString(string sqlCommand, List<SqlParameter> parameters = null, CommandType commandType = CommandType.Text)
        {
            if (_sqlDebug)
            {
                _lastSqlDebugString = SqlGenerator.CreateExecutableSqlStatement(sqlCommand, parameters);
            }
        }
        #endregion
        #region virtual
        protected virtual void BeforeImporting(dynamic items)
        {
        }
        protected virtual void AfterImporting(dynamic items, dynamic flags)
        {
        }
        protected virtual void AfterProcessing(dynamic flags)
        {
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;

namespace Library.Core.Repository.Screenshot
{
    /// <summary>
    /// Special window handles.
    /// </summary>
    public enum SpecialWindowHandles
    {
        // ReSharper disable InconsistentNaming
        /// <summary>
        ///     Places the window at the bottom of the Z order. If the hWnd parameter identifies a topmost window, the window loses its topmost status and is placed at the bottom of all other windows.
        /// </summary>
        HWND_TOP = 0,
        /// <summary>
        ///     Places the window above all non-topmost windows (that is, behind all topmost windows). This flag has no effect if the window is already a non-topmost window.
        /// </summary>
        HWND_BOTTOM = 1,
        /// <summary>
        ///     Places the window at the top of the Z order.
        /// </summary>
        HWND_TOPMOST = -1,
        /// <summary>
        ///     Places the window above all non-topmost windows. The window maintains its topmost position even when it is deactivated.
        /// </summary>
        HWND_NOTOPMOST = -2
        // ReSharper restore InconsistentNaming
    }
    public enum ShowWindowCommands
    {
        //https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow?redirectedfrom=MSDN
        SW_SHOWNORMAL = 1    
    }


    [Flags]
    public enum SetWindowPosFlags : uint
    {
        // ReSharper disable InconsistentNaming

        /// <summary>
        ///     If the calling thread and the thread that owns the window are attached to different input queues, the system posts the request to the thread that owns the window. This prevents the calling thread from blocking its execution while other threads process the request.
        /// </summary>
        SWP_ASYNCWINDOWPOS = 0x4000,

        /// <summary>
        ///     Prevents generation of the WM_SYNCPAINT message.
        /// </summary>
        SWP_DEFERERASE = 0x2000,

        /// <summary>
        ///     Draws a frame (defined in the window's class description) around the window.
        /// </summary>
        SWP_DRAWFRAME = 0x0020,

        /// <summary>
        ///     Applies new frame styles set using the SetWindowLong function. Sends a WM_NCCALCSIZE message to the window, even if the window's size is not being changed. If this flag is not specified, WM_NCCALCSIZE is sent only when the window's size is being changed.
        /// </summary>
        SWP_FRAMECHANGED = 0x0020,

        /// <summary>
        ///     Hides the window.
        /// </summary>
        SWP_HIDEWINDOW = 0x0080,

        /// <summary>
        ///     Does not activate the window. If this flag is not set, the window is activated and moved to the top of either the topmost or non-topmost group (depending on the setting of the hWndInsertAfter parameter).
        /// </summary>
        SWP_NOACTIVATE = 0x0010,

        /// <summary>
        ///     Discards the entire contents of the client area. If this flag is not specified, the valid contents of the client area are saved and copied back into the client area after the window is sized or repositioned.
        /// </summary>
        SWP_NOCOPYBITS = 0x0100,

        /// <summary>
        ///     Retains the current position (ignores X and Y parameters).
        /// </summary>
        SWP_NOMOVE = 0x0002,

        /// <summary>
        ///     Does not change the owner window's position in the Z order.
        /// </summary>
        SWP_NOOWNERZORDER = 0x0200,

        /// <summary>
        ///     Does not redraw changes. If this flag is set, no repainting of any kind occurs. This applies to the client area, the nonclient area (including the title bar and scroll bars), and any part of the parent window uncovered as a result of the window being moved. When this flag is set, the application must explicitly invalidate or redraw any parts of the window and parent window that need redrawing.
        /// </summary>
        SWP_NOREDRAW = 0x0008,

        /// <summary>
        ///     Same as the SWP_NOOWNERZORDER flag.
        /// </summary>
        SWP_NOREPOSITION = 0x0200,

        /// <summary>
        ///     Prevents the window from receiving the WM_WINDOWPOSCHANGING message.
        /// </summary>
        SWP_NOSENDCHANGING = 0x0400,

        /// <summary>
        ///     Retains the current size (ignores the cx and cy parameters).
        /// </summary>
        SWP_NOSIZE = 0x0001,

        /// <summary>
        ///     Retains the current Z order (ignores the hWndInsertAfter parameter).
        /// </summary>
        SWP_NOZORDER = 0x0004,

        /// <summary>
        ///     Displays the window.
        /// </summary>
        SWP_SHOWWINDOW = 0x0040,

        // ReSharper restore InconsistentNaming
    }
    public static class SpecialMonitorFlags
    {
        public const Int32 MONITOR_DEFAULTTOPRIMERTY = 0x00000001;
        public const Int32 MONITOR_DEFAULTTONEAREST = 0x00000002;
    }
    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct NativeRectangle
    {
        public Int32 Left;
        public Int32 Top;
        public Int32 Right;
        public Int32 Bottom;

        public NativeRectangle(Int32 left, Int32 top, Int32 right, Int32 bottom)
        {
            this.Left = left;
            this.Top = top;
            this.Right = right;
            this.Bottom = bottom;
        }
    }
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public sealed class NativeMonitorInfo
    {
        public Int32 Size = Marshal.SizeOf(typeof(NativeMonitorInfo));
        public NativeRectangle Monitor;
        public NativeRectangle Work;
        public Int32 Flags;
    }
    public class NativeMethods
    {
        //************************************************************************************************
        //                                  GetScreenSize
        [DllImport("user32.dll")]
        public static extern IntPtr MonitorFromWindow(IntPtr handle, Int32 flags);
        [DllImport("user32.dll")]
        public static extern Boolean GetMonitorInfo(IntPtr hMonitor, NativeMonitorInfo lpmi);

        public Rectangle GetScreenSize(string title)
        {
            Rectangle screenSize = Rectangle.Empty;

            // Get a handle to the application that matches the title.
            var windowHandle = GetWindowsHandle(title);

            // Verify that the application is a running process.
            if (!windowHandle.HasValue)
            {
                return screenSize;
            }

            var monitor = NativeMethods.MonitorFromWindow(windowHandle.Value, SpecialMonitorFlags.MONITOR_DEFAULTTONEAREST);

            if (monitor != IntPtr.Zero)
            {
                var monitorInfo = new NativeMonitorInfo();
                NativeMethods.GetMonitorInfo(monitor, monitorInfo);

                screenSize = Rectangle.FromLTRB(left: monitorInfo.Monitor.Left, top: monitorInfo.Monitor.Top, right: monitorInfo.Monitor.Right, bottom: monitorInfo.Monitor.Bottom);
            }

            return screenSize;
        }
        //************************************************************************************************
        //                                  BringToFront
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        public void BringToFront(string title)
        {
            // Get a handle to the application that matches the title.
            var handle = GetWindowsHandle(title);

            // Verify that the application is a running process.
            if (!handle.HasValue)
            {
                return;
            }

            // Make the application the foreground application
            SetForegroundWindow(handle.Value);
        }

        //************************************************************************************************
        //                                  SetWindowAppPos
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);
        public void SetWindowAppPos(string title, Rectangle f)
        {
            // Get a handle to the application that matches the title.
            var windowHandle = GetWindowsHandle(title);

            SetWindowPos(windowHandle.Value, (IntPtr)SpecialWindowHandles.HWND_TOPMOST, f.Left, f.Top, f.Right, f.Bottom, SetWindowPosFlags.SWP_SHOWWINDOW);
        }
        //************************************************************************************************
        //                                  GetWindowAppSize
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowRect(HandleRef hWnd, out NativeRectangle lpRect);

        public Rectangle GetWindowAppSize(string title)
        {
            // Get a handle to the application that matches the title.
            var windowHandle = GetWindowsHandle(title);

            NativeRectangle r;
            GetWindowRect(new HandleRef(this, windowHandle.Value), out r);

            return Rectangle.FromLTRB(left: r.Left, top: r.Top, right: r.Right, bottom: r.Bottom);
        }

        //************************************************************************************************
        //                                  GetWindowsHandle
        [DllImport("USER32.DLL", CharSet = CharSet.Unicode)]
        public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);
        public IntPtr? GetWindowsHandle(string title)
        {
            Process[] processesByName = Process.GetProcessesByName(title);
            Process process = processesByName[0];
            IntPtr windowHandle = process.MainWindowHandle;

            // Get a handle to the Calculator application.
            IntPtr handle = FindWindow(null, title);

            // Verify that Calculator is a running process.
            if (windowHandle == IntPtr.Zero)
            {
                return null;
            }

            return windowHandle;
        }

        //************************************************************************************************
        //                                  ShowWindowsHandlers
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, ShowWindowCommands nCmdShow);

        public bool ShowWindowsNormal(string title)
        {
            bool show = true;
            var windowHandle = GetWindowsHandle(title);

            show = ShowWindow(windowHandle.Value, ShowWindowCommands.SW_SHOWNORMAL);

            return show;
        }
    }
}

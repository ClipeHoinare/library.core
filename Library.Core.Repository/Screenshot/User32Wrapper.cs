﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Library.Core.Repository.Screenshot
{
    public class User32Wrapper
    {
        protected NativeMethods _nativeMethods = new NativeMethods();
        protected string _title;

        public User32Wrapper(string title)
        {
            _title = title;
        }

        public Rectangle SetLocationAndSize(Rectangle f)
        {
            _nativeMethods.ShowWindowsNormal(_title);
            _nativeMethods.BringToFront(_title);

            var r = _nativeMethods.GetWindowAppSize(_title);

            //var f = Rectangle.FromLTRB(left: 0, top: 0, right: 1160, bottom: 60);

            if (r.Left != f.Left || r.Top != f.Top || r.Right != f.Right || r.Bottom != f.Bottom)
            {
                _nativeMethods.SetWindowAppPos(_title, f);
            }

            return f;
        }

        public Rectangle GetScreenSize()
        {
            var monitorSize = _nativeMethods.GetScreenSize(_title);

            return monitorSize;
        }
    }

}

﻿using AsyncWindowsClipboard;
using Library.Core.WindowsInput;
using Library.Core.WindowsInput.Native;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms.PlatformConfiguration;
using static Library.Core.Repository.Screenshot.NativeMethods;

namespace Library.Core.Repository.Screenshot
{
    public class ScreenshotHandler
    {
        public Bitmap GetScreenshot(Rectangle rectangle, string filename= "filename")
        {
            var bitmap = new Bitmap(rectangle.Width, rectangle.Height);

            using (var g = Graphics.FromImage(bitmap))
            {
                g.CopyFromScreen(rectangle.X, rectangle.Y, 0, 0, bitmap.Size, CopyPixelOperation.SourceCopy);
            }

            if(filename== "filename")
                filename = "png\\filename" + (DateTime.Now.Minute * 60000 + DateTime.Now.Second * 1000 + DateTime.Now.Millisecond).ToString()+ ".png";

            bitmap.Save(filename, ImageFormat.Png);

            return bitmap;
        }
        /// <summary>
        ///             //send Ctrl+A, Ctrl+C then retrieve from Clipboard
        /// </summary>
        /// <returns></returns>
        public string GetSelectAllTextToAndFromClipboard()
        {
            // Simulate each key stroke
            var sim = new InputSimulator();
            sim.Keyboard
               .Sleep(500)
               .ModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.VK_A)
               .Sleep(200)
               .ModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.VK_C)
               .Sleep(500);

            // get text from clipboard
            var sut = new WindowsClipboardService(TimeSpan.FromMilliseconds(800));
            var actual = sut.GetTextAsync();

            return actual.Result;
        }
        public void TypeTimeValue(string text)
        {
            // Simulate each key stroke
            var sim = new InputSimulator();
            sim.Keyboard
               .Sleep(200)
               .KeyPress(VirtualKeyCode.DELETE)
               .Sleep(500)
               .TextEntry(text)
               .Sleep(200)
               .KeyPress(VirtualKeyCode.RETURN)
               .Sleep(200)
               .KeyPress(VirtualKeyCode.TAB)
               .Sleep(200)
               ;

            //// get text from clipboard
            //var sut = new WindowsClipboardService(TimeSpan.FromMilliseconds(500));
            //var actual = sut.GetTextAsync();

            //return actual.Result;
        }
        public void ClickAndSelectAt(Point pt)
        {
            var sim = new InputSimulator();
            sim.Mouse
               .MoveMouseTo(pt.X, pt.Y)
               .Sleep(500)
               .LeftButtonClick()
               .Sleep(200)
               .LeftButtonDoubleClick()
               .Sleep(500);
        }
        public void ClickAt(Point pt)
        {
            var sim = new InputSimulator();
            sim.Mouse
               .MoveMouseTo(pt.X, pt.Y)
               .Sleep(500)
               .LeftButtonClick()
               .Sleep(700);
        }

    }

    public class ChromeWrapper
    {
        //https://stackoverflow.com/questions/30156913/sending-keyboard-key-to-browser-in-c-sharp-using-sendkey-function
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        // the keystroke signals. you can look them up at the msdn pages
        private static uint WM_KEYDOWN = 0x100, WM_KEYUP = 0x101;

        // the reference to the chrome process
        private Process chromeProcess;

        public ChromeWrapper(string url)
        {
            //Process.Start("chrome.exe", url); //no need to keep reference to this process, because if chrome is already opened, this is NOT the correct reference.
            Thread.Sleep(600);

            Process[] procsChrome = Process.GetProcessesByName("thinkorswim");
            foreach (Process chrome in procsChrome)
            {
                if (chrome.MainWindowHandle == IntPtr.Zero)// the chrome process must have a window
                    continue;
                chromeProcess = chrome; //now you have a handle to the main chrome (either a new one or the one that was already open).
                return;
            }
        }

        public void SendKey(char key)
        {
            try
            {
                if (chromeProcess.MainWindowHandle != IntPtr.Zero)
                {

                    int WM_SYSCOMMAND = 0x112;
                    int SC_MAXIMIZE = 0xf030;
                    SendMessage(chromeProcess.MainWindowHandle, (uint)WM_SYSCOMMAND, (IntPtr)SC_MAXIMIZE, IntPtr.Zero);

                    // send the keydown signal
                    SendMessage(chromeProcess.MainWindowHandle, ChromeWrapper.WM_KEYDOWN, (IntPtr)key, IntPtr.Zero);
                    // give the process some time to "realize" the keystroke
                    Thread.Sleep(30); //On my system it works fine without this Sleep.
                                      // send the keyup signal IntPtr
                    SendMessage(chromeProcess.MainWindowHandle, ChromeWrapper.WM_KEYUP, (IntPtr)key, IntPtr.Zero);
                }
            }
            catch (Exception e) //without the GetProcessesByName you'd get an exception.
            {
            }
        }
    }
    public class WindowsWorker
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessageTimeout", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern uint SendMessageTimeoutText(IntPtr hWnd, int Msg, int countOfChars, StringBuilder text, uint flags, uint uTImeoutj, uint result);


        const uint WM_SETTEXT = 0x000C;

        public static string GetText()//(IntPtr hwnd)
        {

            Process[] processesByName = Process.GetProcessesByName("thinkorswim");
            Process process = processesByName[0];
            if (process == null)
            {
                Console.WriteLine("Process not found");
                return null;
            }
            IntPtr windowHandle = process.MainWindowHandle;


            var text = new StringBuilder(1024);
            if (SendMessageTimeoutText(windowHandle, 0xd, 1024, text, 0x2, 1000, 0) != 0)
            {
                return text.ToString();
            }

            return "";
        }

        public static void SetText(IntPtr HWnd, string strTextToSet)
        {
            //var text = new StringBuilder(strTextToSet);
            IntPtr text = System.Runtime.InteropServices.Marshal.StringToCoTaskMemUni(strTextToSet);
            SendMessage(HWnd, WM_SETTEXT, IntPtr.Zero, text);
        }
    }
    class MainClass
    {
        //Just to reduce amont of log itmes
        static HashSet<Tuple<string, int>> cache = new HashSet<Tuple<string, int>>();
        public static void LogThread(string msg, bool clear = false)
        {
            if (clear)
                cache.Clear();
            var val = Tuple.Create(msg, Thread.CurrentThread.ManagedThreadId);
            if (cache.Add(val))
                Console.WriteLine("{0}\t:{1}", val.Item1, val.Item2);
        }

        public static async Task<int> FindSeriesSum(int i1)
        {
            LogThread("Task enter");
            int sum = 0;
            for (int i = 0; i < i1; i++)
            {
                sum += i;
                if (i % 1000 == 0)
                {
                    LogThread("Before yield");
                    await Task.Yield();
                    LogThread("After yield");
                }
            }
            LogThread("Task done");
            return sum;
        }

        public static void Main(string[] args)
        {
            LogThread("Before task");
            var task = FindSeriesSum(1000000);
            LogThread("While task", true);
            Console.WriteLine("Sum = {0}", task.Result);
            LogThread("After task");
        }
    }
}
//Resources:
//http://automation-home.blogspot.com/2014/09/AutomateCalcAppWithJavaAndSikuli.html
//https://subscription.packtpub.com/book/application_development/9781782167877/1/ch01lvl1sec06/top-13-features-you-need-to-know-about
//https://github.com/christianrondeau/SikuliSharp
//https://www.logigear.com/blog/test-automation/12-best-automation-tools-for-desktop-apps-in-2020/
//https://www.codeproject.com/Questions/1167517/How-to-send-CTRLplusC-keys-to-windows-in-Csharp
//https://stackoverflow.com/questions/18898748/function-sendmessage-of-user32-dll-does-not-work-in-windows-8
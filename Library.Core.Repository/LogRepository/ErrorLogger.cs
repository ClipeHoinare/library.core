﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repository.LogRepository
{

    public enum ErrorLevel
    {
        Trace = 1, // ("Trace: The chatter of people on the street");
        Debug = 2, // ("Debug: Where are you going and why?");
        Informational = 3, //("Info: What bus station you're at.");
        Warn = 4, //("Warn: You're playing on the phone and not looking up for your bus");
        Error = 5, //("Error: You get on the wrong bus.");
        Fatal = 6  //("Fatal: You are run over by the bus.");
    }

    public struct ErrorLoggerName
    {
        public const string NotDefined = "";
        public const string Worker = "Worker";
        public const string YahooBox = "YahooBox";
        public const string TosBox = "TosBox";
        public const string StorageSqlQueue = "StorageSqlQueue";

    }

    public class ErrorLogger : IErrorLogger
    {
        public static void LogException(Exception exception, ErrorLevel errorLevel, string message)
        {
            LogException(exception, errorLevel, ErrorLoggerName.NotDefined, message);
        }

        public static void LogException(Exception exception, ErrorLevel errorLevel, string loggerName, string message)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            LogEventInfo eventInfo = new LogEventInfo(ErrorLevelToLogError(errorLevel), loggerName, message);
            eventInfo.Properties["exceptionstacktrace"] = exception.StackTrace == null ? "" : exception.StackTrace.ToString();
            eventInfo.Exception = exception;
            logger.Log(eventInfo);
        }

        protected static LogLevel ErrorLevelToLogError(ErrorLevel errorLevel)
        {
            switch ( errorLevel)
            {
                case ErrorLevel.Trace:
                    return LogLevel.Trace;
                case ErrorLevel.Debug:
                    return LogLevel.Debug;
                case ErrorLevel.Informational:
                    return LogLevel.Info;
                case ErrorLevel.Warn:
                    return LogLevel.Warn;
                case ErrorLevel.Error:
                    return LogLevel.Error;
                case ErrorLevel.Fatal:
                    return LogLevel.Fatal;
            }

            return LogLevel.Fatal;
        }

    }
}

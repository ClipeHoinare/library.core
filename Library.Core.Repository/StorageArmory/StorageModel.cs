﻿using Library.Domain.Jackdow;
using Library.Repository.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Library.Repository.StorageArmory
{
    public static partial class StorageModelORM
    {

        #region public extensions for IModel

        public static StorageSqlQueue.SqlBundle ModelToSqlBundle(this IModelTable modelTable, ModelTableType buildType = ModelTableType.Both)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            StringBuilder sbFields = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            StringBuilder sbUpdate = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();

            // get all properties from the type
            var typeModel = modelTable.GetType();
            var propertyInfos = typeModel.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            var tableName = typeModel.CustomAttributes.Where(x => x.AttributeType.Name == "TableAttribute").ToList()[0].ConstructorArguments[0].Value.ToString();
            var schema = typeModel.CustomAttributes.Where(x => x.AttributeType.Name == "TableAttribute" && x.NamedArguments[0].MemberName == "Schema").ToList()[0].NamedArguments[0].TypedValue.Value.ToString();
            Enum.TryParse(schema, out ClassFactory.DenKey denKey);

            // loop thru all the properties of the object and fill the string builders
            foreach (var propertyInfo in propertyInfos)
            {
                var name = propertyInfo.Name;
                var value = propertyInfo.GetValue(modelTable, null);

                if (value == null)
                    continue;

                var includeField = false;

                var flags = GetCustomFlags(modelTable, propertyInfo);

                if (!(flags.IsIdentity || flags.IsDefault || !flags.IsEditable))
                {
                    sbValues.Append(", @" + name);
                    sbFields.Append(", " + name);
                    includeField = true;
                }
                if (!(flags.IsIdentity || flags.IsKey || !flags.IsEditable))
                {
                    sbUpdate.Append(", " + name + " = @" + name);
                    includeField = true;
                }

                if (flags.IsKey)
                {
                    sbWhere.Append(" and " + name + " = @" + name);
                    includeField = true;
                }

                if (includeField)
                {
                    var param = new SqlParameter(name, value);

                    parameters.Add(param);
                }
            }

            if (sbFields.Length > 0) { sbFields.Remove(0, 1); }
            if (sbValues.Length > 0) { sbValues.Remove(0, 1); }
            if (sbUpdate.Length > 0) { sbUpdate.Remove(0, 1); }
            if (sbWhere.Length > 0) { sbWhere.Remove(0, 4); }

            //start building the command string
            var sqlCommand = string.Empty;

            var sqlCommandInsert = "INSERT INTO " + tableName + @" 
                (" + sbFields.ToString() + @")
                values (" + sbValues.ToString() + ");";

            var sqlCommandUpdate = string.Empty;

            if (sbWhere.Length > 0)
            {
                sqlCommandUpdate = "UPDATE " + tableName + @" SET
                " + sbUpdate.ToString() + @" 
                    WHERE " + sbWhere.ToString() +";";
            }

            if (buildType == ModelTableType.Insert)
            {
                sqlCommand = sqlCommandInsert;
            }
            else if (buildType == ModelTableType.Update && sbWhere.Length > 0)
            {
                sqlCommand = sqlCommandUpdate;
            }
            else if (buildType == ModelTableType.Both && sbWhere.Length > 0)
            {
                sqlCommand = " IF NOT EXISTS (SELECT * from " + tableName + " WHERE " + sbWhere.ToString() + @")
                " + sqlCommandInsert + @"
                    else 
                " + sqlCommandUpdate +"";
            }

            // build the sqlBundle
            var sqlBundle = new StorageSqlQueue.SqlBundle(denKey) { CommandText = sqlCommand, Parameters = parameters };

            return sqlBundle;
        }

        internal static SqlDbType GetSqlDbType(Type systype)
        {
            SqlDbType resulttype = SqlDbType.NVarChar;
            Dictionary<Type, SqlDbType> Types = new Dictionary<Type, SqlDbType>();
            Types.Add(typeof(Boolean), SqlDbType.Bit);
            Types.Add(typeof(String), SqlDbType.NVarChar);
            Types.Add(typeof(DateTime), SqlDbType.DateTime);
            Types.Add(typeof(Int16), SqlDbType.Int);
            Types.Add(typeof(Int32), SqlDbType.Int);
            Types.Add(typeof(Int64), SqlDbType.Int);
            Types.Add(typeof(Decimal), SqlDbType.Float);
            Types.Add(typeof(Double), SqlDbType.Float);
            Types.TryGetValue(systype, out resulttype);
            return resulttype;
        }

        public static List<T> ToList<T>(this DataTable table) where T : class, IModelTable, new()
        {
            // get all properties from the type
            var typeModel = typeof(T);
            var propertyInfos = typeModel.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            var result = new List<T>();
            foreach (var row in table.AsEnumerable())
            {
                T modelTable = new T();

                // loop thru all the properties of the object and fill the string builders
                foreach (var propertyInfo in propertyInfos)
                {
                    var flags = GetCustomFlags(modelTable, propertyInfo);

                    if (flags.IsMapped)
                    {
                        try
                        {
                            var name = propertyInfo.Name;
                            propertyInfo.SetValue(modelTable, ChangeType(row[propertyInfo.Name], propertyInfo.PropertyType), null);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }
                }

                result.Add(modelTable);
            }

            return result;
        }

        #endregion

        #region private area

        private static CustomFlags GetCustomFlags(IModelTable modelTable, PropertyInfo propertyInfo)
        {
            var customFlags = new CustomFlags();

            try
            {
                var modelTableType = modelTable.GetType();

                var modelTableMetadataType = Type.GetType(modelTableType.FullName + "Metadata, " + modelTableType.Assembly.FullName);

                if (modelTableMetadataType != null)
                {
                    var propertyMetaInfo = modelTableMetadataType.GetProperty(propertyInfo.Name);
                    if (propertyMetaInfo != null)
                    {
                        var generatedAttribute = propertyMetaInfo.GetCustomAttributes(false).Where(x => x.GetType().Equals(typeof(DatabaseGeneratedAttribute))).FirstOrDefault();
                        if (generatedAttribute != null)
                        {
                            if (((DatabaseGeneratedAttribute)generatedAttribute).DatabaseGeneratedOption == DatabaseGeneratedOption.Identity)
                                customFlags.IsIdentity = true;

                            if (((DatabaseGeneratedAttribute)generatedAttribute).DatabaseGeneratedOption == DatabaseGeneratedOption.Computed)
                                customFlags.IsDefault = true;
                        }

                        var editableAttribute = propertyMetaInfo.GetCustomAttributes(false).Where(x => x.GetType().Equals(typeof(EditableAttribute))).FirstOrDefault();
                        if (editableAttribute != null)
                        {
                            if (!((EditableAttribute)editableAttribute).AllowEdit)
                                customFlags.IsEditable = false;
                        }

                        var keyAttribute = propertyMetaInfo.GetCustomAttributes(false).Where(x => x.GetType().Equals(typeof(KeyAttribute))).FirstOrDefault();
                        if (keyAttribute != null)
                        {
                            customFlags.IsKey = true;
                        }

                        var notMappedAttribute = propertyMetaInfo.GetCustomAttributes(false).Where(x => x.GetType().Equals(typeof(NotMappedAttribute))).FirstOrDefault();
                        if (notMappedAttribute != null)
                        {
                            customFlags.IsMapped = false;
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return customFlags;
        }

        private class CustomFlags
        {
            public bool IsIdentity { set; get; }
            public bool IsDefault { set; get; }
            public bool IsEditable { set; get; } = true;
            public bool IsKey { set; get; }
            public bool IsMapped { set; get; } = true;
        }

        public static object ChangeType(object value, Type conversion)
        {
            var t = conversion;

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }

                t = Nullable.GetUnderlyingType(t);
            }

            return Convert.ChangeType(value, t);
        }

        #endregion

        #region not used yet

        public static string GetTableIdentityColumn(IModelTable modelTable)
        {
            throw new NotImplementedException();
        }

        public static T Cast<T>(this IModelTable myobj)
        {
            Type objectType = myobj.GetType();
            Type target = typeof(T);
            var x = Activator.CreateInstance(target, false);
            var z = from source in objectType.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            var d = from source in target.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;
            List<MemberInfo> members = d.Where(memberInfo => d.Select(c => c.Name)
               .ToList().Contains(memberInfo.Name)).ToList();
            PropertyInfo propertyInfo;
            object value;
            foreach (var memberInfo in members)
            {
                propertyInfo = typeof(T).GetProperty(memberInfo.Name);
                value = myobj.GetType().GetProperty(memberInfo.Name).GetValue(myobj, null);

                propertyInfo.SetValue(x, value, null);
            }
            return (T)x;
        }

        #endregion

        public static class SqlHelper
        {
            private static Dictionary<Type, SqlDbType> typeMap;

            // Create and populate the dictionary in the static constructor
            static SqlHelper()
            {
                typeMap = new Dictionary<Type, SqlDbType>();

                typeMap[typeof(string)] = SqlDbType.NVarChar;
                typeMap[typeof(char[])] = SqlDbType.NVarChar;
                typeMap[typeof(byte)] = SqlDbType.TinyInt;
                typeMap[typeof(short)] = SqlDbType.SmallInt;
                typeMap[typeof(int)] = SqlDbType.Int;
                typeMap[typeof(long)] = SqlDbType.BigInt;
                typeMap[typeof(byte[])] = SqlDbType.Image;
                typeMap[typeof(bool)] = SqlDbType.Bit;
                typeMap[typeof(DateTime)] = SqlDbType.DateTime2;
                typeMap[typeof(DateTimeOffset)] = SqlDbType.DateTimeOffset;
                typeMap[typeof(decimal)] = SqlDbType.Money;
                typeMap[typeof(float)] = SqlDbType.Real;
                typeMap[typeof(double)] = SqlDbType.Float;
                typeMap[typeof(TimeSpan)] = SqlDbType.Time;
                /* ... and so on ... */
            }

            // Non-generic argument-based method
            public static SqlDbType GetDbType(Type giveType)
            {
                // Allow nullable types to be handled
                giveType = Nullable.GetUnderlyingType(giveType) ?? giveType;

                if (typeMap.ContainsKey(giveType))
                {
                    return typeMap[giveType];
                }

                throw new ArgumentException($"{giveType.FullName} is not a supported .NET class");
            }

            // Generic version
            public static SqlDbType GetDbType<T>()
            {
                return GetDbType(typeof(T));
            }
        }
    }
}

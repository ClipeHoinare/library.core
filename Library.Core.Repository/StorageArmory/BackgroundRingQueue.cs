﻿using Disruptor;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Repository.BaseRepository
{
    /// <summary>
    /// Perform asynchronous processing in a background thread.
    /// </summary>
    /// <remarks>
    /// Handle thread creation, exit, thread safety.
    /// https://codereview.stackexchange.com/questions/151562/loop-for-periodic-processing-in-a-background-thread
    /// 
    /// Enhanced to include Disruptor ring pattern with parallel processing
    /// https://dzone.com/articles/performance-evaluation-disruptor-with-parallel-con
    /// 
    /// Items will stay in the queue until the ring will be able to get them
    /// </remarks>
    public interface IBackgroundRingQueue<T> : IDisposable
    {
        //ForFuture: use abstract class to declare the implementation of the constructor...
        //void IBackgroundRingQueue(BackgroundRingQueue<T>.ValueAdditionHandler<T>.ProcessItem processItemDelegate, int ringSize, int handlerSize);

        bool AddItem(T item);

        /// <summary> Count of the elements in the queue before being added to the ring </summary>
        int Count { get; }

        /// <summary>size of the disruptor ring.</summary>
        int RingSize { get; }

        /// <summary>number of the events for processing.</summary>
        int HandlerSize { get; }
    }

    /// Perform asynchronous parallel processing in a background thread.
    /// receives object to be prosessed and processing method delegate
    public class BackgroundRingQueue<T> : IBackgroundRingQueue<T>
    {
        public class ValueEntry<S>
        {
            /// <summary> status of the current ValueEntry; as the object is reusable we need to implement couple procedures </summary>
            public enum StatusEnum
            {
                Created,
                Processing,
                Processed
            }

            private S _value;
            /// <summary>
            /// Value passed by the calling procedure
            /// </summary>
            public S Value
            {
                get
                {
                    return _value;
                }
                set
                {
                    _value = value;
                    // flag it as a clean object to be processed by the next time is published
                    Status = StatusEnum.Created;
                }
            }

            //flag to be able to be processed by only one event
            public object lockValueEntry = new object();

            /// <summary>
            /// Current status of the "reusable" object.
            /// </summary>
            public StatusEnum Status { get; set; }
            /// <summary>
            /// Constructor
            /// </summary>
            public ValueEntry()
            {
                //Console.WriteLine("New ValueEntry created");
            }

            /// <summary>
            /// Try to get this EntryValue for processing, return true; or return false if EntryValue is already taken.
            /// It is used in the ValueAdditionHandler
            /// </summary>
            /// <returns></returns>
            public bool GetExclusivity()
            {
                bool used = true;
                if (Status.Equals(StatusEnum.Created))
                {
                    lock (lockValueEntry)
                    {
                        if (Status.Equals(StatusEnum.Created))
                        {
                            Status = StatusEnum.Processing;
                            used = false;
                        }
                    }
                }
                return !used;
            }

            /// <summary>
            /// Set processed status
            /// </summary>
            public void SetProcessedStatus()
            {
                Status = StatusEnum.Processed;
            }
        }

        public class ValueAdditionHandler<S> : IEventHandler<ValueEntry<S>>
        {
            // process item from the ring
            public delegate void ProcessItem(S item);

            internal delegate void ProcessClosure();

            //Guid of the current process, not used
            private readonly string _guid;

            // delegate placeholder for the method passed by the calling process
            private ProcessItem _processItemDelegate;

            private ProcessClosure _processClosure;

            //create a new event handler
            internal ValueAdditionHandler(ProcessItem processItemDelegate, ProcessClosure processClosure)
            {
                _guid = Guid.NewGuid().ToString();
                _processItemDelegate = processItemDelegate;
                _processClosure = processClosure;
            }

            // implementing the OnEvent of the IEventHandler interface 
            public void OnEvent(ValueEntry<S> data, long sequence, bool endOfBatch)
            {
                // if the ValueEntry was taken by a different event just get out
                if (!data.GetExclusivity())
                    return;

                //here we process the value with the method delegate passed in at "Start"
                _processItemDelegate?.Invoke(data.Value);

                //flag ValueEntry as processed (not necessary at this moment)
                data.SetProcessedStatus();
                //Console.WriteLine("Event handled: Value = {0} (processed event {1})(consumer: {2})", data.Value.ToString(), sequence, _guid);

                OnComplete();
            }

            internal void OnComplete()
            {
                _processClosure?.Invoke();
            }
        }

        /// <summary>Item handling delegate.</summary>
        private ValueAdditionHandler<T>.ProcessItem _processItemDelegate;

        /// <summary>Queue for the incoming items.</summary>
        private ConcurrentQueue<T> _queue = new ConcurrentQueue<T>();

        /// <summary> lock object for incrementing counts </summary>
        private object lockCounting = new object();

        /// <summary>Task for background processing.</summary>
        private Task _backgroundRingTask;

        /// <summary> disruptor ring storage</summary>
        private RingBuffer<ValueEntry<T>> _ringBuffer;

        /// <summary> disruptor dsl pattern</summary>
        private Disruptor.Dsl.Disruptor<ValueEntry<T>> _disruptor;

        /// <summary>Event for awakening the processing loop, used when new entry is added to the queue or exit requested.</summary>
        private AutoResetEvent _queueEvent = new AutoResetEvent(false);

        /// <summary>Flag to signal stop for the parallel sender thread.</summary>
        private bool _stopTaskFlag;

        /// <summary>Flag to store if this class is disposing</summary>
        private bool _disposed;

        //Descriptions at the references fields/methods
        private int _handlerSize = 16;
        private int _ringSize = 32;

        /// <summary>keep count of the consumed items</summary>
        public long CountConsumed;
        /// <summary>keep count of the published items</summary>
        public long CountProduced;

        /// <summary>Increment Consumed items count...</summary>
        internal void CountingConsumed()
        {
            lock (lockCounting)
            {
                CountConsumed++;
                _queueEvent?.Set();
            }
        }

        /// <summary>Wait for all items to be processed or for the timeout</summary>
        public void WaitForAll(int timeoutMinutes = 3)
        {
            var sw1 = Stopwatch.StartNew();
            while (true)
            {
                _queueEvent.WaitOne(4000);

                if (Count <= 0 && CountConsumed == CountProduced)
                {
                    _queueEvent.WaitOne(1000);
                    break;
                }

                if (sw1.Elapsed.TotalMinutes > timeoutMinutes)
                {
                    break;
                }
            }
        }

        /// <summary>size of the disruptor ring.</summary>
        public int RingSize => _ringSize;

        /// <summary>number of the events for processing.</summary>
        public int HandlerSize => _handlerSize;

        /// <summary> Count of the elements in the queue before being added to the ring </summary>
        public int Count => _queue.Count;

        /// <summary>
        /// Constructor of the ring
        /// </summary>
        /// <param name="ringSize">ring size</param>
        /// <param name="handlerSize">number of events for processing</param>
        public BackgroundRingQueue(ValueAdditionHandler<T>.ProcessItem processItemDelegate, int ringSize = 32, int handlerSize = 24)
        {
            _ringSize = ringSize;
            _handlerSize = handlerSize;

            _disruptor = new Disruptor.Dsl.Disruptor<ValueEntry<T>>(() => new ValueEntry<T>(), _ringSize, TaskScheduler.Default);
            var handlers = new ValueAdditionHandler<T>[_handlerSize];
            for (int i = 0; i < _handlerSize; i++)
            {
                handlers[i] = new ValueAdditionHandler<T>(processItemDelegate, CountingConsumed);
            }
            _disruptor.HandleEventsWith(handlers);
            _ringBuffer = _disruptor.Start();

            _processItemDelegate = processItemDelegate;
            _backgroundRingTask = Task.Factory.StartNew(BackgroundQueueToRing, TaskCreationOptions.LongRunning);
        }

        /// <summary>
        /// Adds new item to the queue
        /// </summary>
        /// <param name="item">New item to add to the queue.</param>
        public bool AddItem(T item)
        {
            bool added = false;
            if (!_stopTaskFlag) //// don't add any more if stop has been requested
            {
                _queue?.Enqueue(item);
                _queueEvent?.Set();
                added = true;
            }
            return added;
        }

        public void ReStart()
        {
            _stopTaskFlag = false;
            _queueEvent.Set();
        }

        #region private area
        /// <summary>
        /// Increment published items count
        /// </summary>
        private void CountingProduced()
        {
            lock (lockCounting)
            {
                CountProduced++;
            }
        }
        /// <summary>
        /// Stops the sender parallel task, but send all data before exiting.
        /// </summary>
        private void StopSenderTask()
        {
            // set the stop flag, and signal change
            _stopTaskFlag = true;
            _queueEvent.Set();
            // wait till the loop exits
            WaitForAll(60);
            _backgroundRingTask.Wait();
            //Console.WriteLine("Thread exited");
        }

        /// <summary>
        /// Infinite loop for processing the incoming events from the Queue.
        /// </summary>
        private void BackgroundQueueToRing()
        {
            while (true)
            {
                while (_queue.Count > 0)
                {
                    //check availability in the ring
                    if (_ringBuffer.GetRemainingCapacity() <= 0)
                    {
                        // wait for some time and then try again
                        _queueEvent.WaitOne(500);
                        continue;
                    }

                    //get next sequence in the ring buffer and if possible publish the next item to it
                    long sequenceNo;
                    if (_ringBuffer.TryNext(out sequenceNo))
                    {
                        if (_queue.TryDequeue(out T item))
                        {
                            if (item != null)
                            {
                                var entry = _ringBuffer[sequenceNo];
                                entry.Value = item;
                                _ringBuffer.Publish(sequenceNo);
                                CountingProduced();
                            }
                        }
                    }
                    else
                    {
                        // wait for some time and then try again
                        _queueEvent.WaitOne(500);
                    }
                }

                // Stop if need to stop and queue has been emptied
                if (_stopTaskFlag)
                {
                    if (_queue.Count == 0)
                    {
                        break;
                    }
                    // stop requested but not empty yet -- loop without waiting
                    continue;
                }

                // Block for a while, until an event happens.  The exact timeout value does not really matter
                _queueEvent.WaitOne(5000);
            }
            //Console.WriteLine("Thread exiting...");
        }
        #endregion
        #region dispose

        /// <summary>
        /// Stop the background thread to receive more requests, and dispose of the thread after that
        /// </summary>
        public void Dispose()
        {
            //Console.WriteLine("Request to dispose Thread");
            StopSenderTask();
            _queueEvent.Dispose();
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}

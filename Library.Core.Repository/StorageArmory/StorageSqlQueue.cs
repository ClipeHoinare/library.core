using Library.Repository.Base;
using Library.Repository.BaseRepository;
using Library.Repository.LogRepository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Library.Repository.StorageArmory
{
    public class StorageSqlQueue
    {
        public class SqlBundle
        {
            public ClassFactory.DenKey DenValue;
            public string CommandText;
            public List<SqlParameter> Parameters = null;
            public CommandType CommandType = CommandType.Text;

            public override string ToString()
            {
                string _identity = string.Empty;
                if (Parameters != null && Parameters.Count > 0)
                {
                    //ForFuture include first 32 bits from each parameter
                    _identity = Parameters[0].Value != DBNull.Value ? Parameters[0].Value.ToString() : string.Empty;
                }

                return _identity ?? string.Empty;
            }

            /// <summary>
            /// Used mostly for debugging, it is creating a blob to select all the parameters values in sql query window
            /// </summary>
            /// <returns></returns>
            public string ParamToString()
            {
                StringBuilder declareBuilder = new StringBuilder();
                StringBuilder selectBuilder = new StringBuilder();
                for (int i = 0; i < Parameters.Count; i++)
                {
                    var param = Parameters[i];
                    declareBuilder.Append("DECLARE @" + param.ParameterName + " " + param.SqlDbType);
                    if (param.SqlDbType.Equals(SqlDbType.Decimal))
                    {
                        declareBuilder.Append(" (" + (((System.Data.SqlTypes.SqlDecimal)param.SqlValue).Precision + ((System.Data.SqlTypes.SqlDecimal)param.SqlValue).Scale).ToString() + "," + ((System.Data.SqlTypes.SqlDecimal)param.SqlValue).Scale.ToString() + ")");
                    }
                    else if (new[] { SqlDbType.Char, SqlDbType.NChar, SqlDbType.NText, SqlDbType.NVarChar, SqlDbType.Text, SqlDbType.VarChar }.Contains(param.SqlDbType))
                    {
                        declareBuilder.Append(" (" + ( param.Size.ToString() != null ? param.Size.ToString() : "0") + ")");
                    }

                    declareBuilder.AppendLine(" = '" + ((param.Value == DBNull.Value || param.Value == null) ? "" : param.Value.ToString()) + "'");
                    selectBuilder.Append(", @" + param.ParameterName + " " + param.ParameterName);
                }
                declareBuilder.AppendLine("").AppendLine("");
                selectBuilder.AppendLine("").AppendLine("");
                var result = "SELECT " + selectBuilder.ToString(1, selectBuilder.Length - 1);
                result = declareBuilder.ToString() + result;

                return result;
            }

            public SqlBundle(ClassFactory.DenKey denValue)
            {
                DenValue = denValue;
            }
        }

        public static BackgroundRingQueue<SqlBundle> RingQueue;

        public void ExecuteQueueMessage(SqlBundle bundle)
        {
            string a = string.Empty;
            string b = string.Empty;
            try
            {
                var factory = (new ClassFactory(bundle.DenValue));
                a = bundle.ParamToString();
                b = SqlGenerator.CreateExecutableSqlStatement(bundle.CommandText, bundle.Parameters);
                factory.ExecuteNonQuery(bundle.CommandText, bundle.Parameters, bundle.CommandType);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogException(ex, ErrorLevel.Error, ErrorLoggerName.StorageSqlQueue, String.Format("StorageSqlQueue.ExecuteQueueMessage, bundle.ToString()={0}", bundle.ToString()));
            }
        }
        public void Start()
        {
            // start disruptor ring background process
            if (RingQueue == null)
                RingQueue = new BackgroundRingQueue<SqlBundle>(ExecuteQueueMessage, 32, 24);
            else
                RingQueue.ReStart();
        }

        public void Stop()
        {
            RingQueue.Dispose();
            RingQueue = null;
        }
    }
}
﻿//using Library.Repository.StorageArmory;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Library.Repository.Base
{
    public class ClassFactory
    {
        public const string CrLf = "\n\r";

        public enum DenKey
        {
            AssetDen,
            HoardDen,
            QuestDen,
            StudyDen,
            TriadDen,
            OptionMetrics
        }

        private string _sqlString = null;

        private string _connectionStringKey;
        private int _timeout;

        public string ConnectionString
        {
            get
            {
                string connectionString = ConfigurationManager.ConnectionStrings[_connectionStringKey].ConnectionString;

                return connectionString;
            }
        }

        public string SqlString
        {
            get
            {
                return _sqlString;
            }
        }

        public ClassFactory(DenKey connectionKey, int timeout = 300)
        {
            _connectionStringKey = connectionKey.ToString();
            _timeout = timeout;
        }

        public SqlParameterCollection ExecuteNonQuery(string commandText, List<SqlParameter> parameters = null, CommandType commandType = CommandType.Text, bool generateSql = false)
        {
            int count = 0;

            if (generateSql)
                _sqlString = SqlGenerator.CreateExecutableSqlStatement(commandText, parameters);
            else
                _sqlString = null;

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = commandType;
            command.CommandTimeout = _timeout;

            if (parameters != null)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.AddWithNullValue(parameter);
                }
            } // parameters != null

            try
            {
                connection.Open();
                count = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //command.Parameters.Clear();
                command.Dispose();
                connection.Close();
                connection.Dispose();
            }

            return command.Parameters;
        }

        public object ExecuteScalar(string commandText, List<SqlParameter> parameters = null, CommandType commandType = CommandType.Text, bool generateSql = false)
        {
            object obj = null;

            if (generateSql)
                _sqlString = SqlGenerator.CreateExecutableSqlStatement(commandText, parameters);
            else
                _sqlString = null;

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = commandType;
            command.CommandTimeout = _timeout;

            if (parameters != null)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.AddWithNullValue(parameter);
                }
            } // parameters != null

            try
            {
                connection.Open();
                obj = command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                connection.Close();
                connection.Dispose();
            }

            return obj;
        }

        public DataSet GetDataSet(string commandText, List<SqlParameter> parameters = null, System.Data.CommandType commandType = CommandType.Text, bool generateSql = false)
        {
            DataSet dataSet = new DataSet();

            if (generateSql)
                _sqlString = SqlGenerator.CreateExecutableSqlStatement(commandText, parameters);
            else
                _sqlString = null;

            SqlConnection connection = new SqlConnection(ConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = commandType;
            command.CommandTimeout = _timeout;

            if (parameters != null)
            {
                foreach (SqlParameter parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }
            } // parameters != null

            try
            {
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(command);
                
                da.Fill(dataSet);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                command.Dispose();
                connection.Close();
                connection.Dispose();
            }

            return dataSet;
        }

        /// <summary>
        /// Return a list of objects
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> ConvertToList(DataSet ds, int tableId = 0)
        {
            //convert datatable to list using LINQ. Input datatable is "dt"
            List<Dictionary<string, object>> lst = ds.Tables[tableId].AsEnumerable()
                .Select(r => r.Table.Columns.Cast<DataColumn>()
                        .Select(c => new KeyValuePair<string, object>(c.ColumnName, r[c.Ordinal])
                       ).ToDictionary(z => z.Key, z => z.Value)
                ).ToList();

            return lst;
        }
    }
}
